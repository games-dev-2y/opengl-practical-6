#pragma once

#include "Vector3.h"

class Matrix3
{
	friend Matrix3 operator+(const Matrix3&, const Matrix3&);
	friend Matrix3 operator-(const Matrix3&, const Matrix3&);
	friend Matrix3 operator*(const Matrix3&, const double&);
	friend Vector3 operator*(const Matrix3&, const Vector3&);
	friend Matrix3 operator*(const Matrix3&, const Matrix3&);

public:
	enum class Axis { X, Y, Z };

	Matrix3();
	Matrix3(Vector3, Vector3, Vector3);
	Matrix3(double, double, double, double, double, double, double, double, double);

	static Matrix3 transpose(const Matrix3&);
	static Matrix3 inverse(const Matrix3&);
	static Matrix3 rotation(const Axis&, const double&);
	static Matrix3 translate(const double&, const double&);
	static Matrix3 scale(const double&, const double&, const double& z = 1.0);

	double determinant() const;
	Vector3 row(const int&) const;
	Vector3 column(const int&) const;
	std::string toString() const;

private:
	double m_A11; // row 1 col 1
	double m_A12; // row 1 col 2
	double m_A13; // row 1 col 3
	double m_A21; // row 2 col 1
	double m_A22; // row 2 col 2
	double m_A23; // row 2 col 3
	double m_A31; // row 3 col 1
	double m_A32; // row 3 col 2
	double m_A33; // row 3 col 3
};

