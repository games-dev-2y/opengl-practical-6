#include <iostream>
#include "SFML/Window.hpp"
#include "GL/glew.h"
#include "SFML/OpenGL.hpp"
#include <gl/GL.h>
#include <gl/GLU.h>

#include "Matrix3.h"

class Game
{
public:
	Game();
	~Game();
	void run();
private:
	bool isRunning = false;
	void processGameEvents(const sf::Event &);
	void update();
	void updateVertices();
	void draw();

	const int primitives;

	const Vector3 COLOR_WHITE = Vector3(1.0, 1.0, 1.0);
	const Vector3 COLOR_GREEN = Vector3(0.0, 1.0, 0.0);
	const Vector3 COLOR_BLUE = Vector3(0.0, 0.0, 1.0);
	const Vector3 COLOR_RED = Vector3(1.0, 0.0, 0.0);
	const Vector3 COLOR_YELLOW = Vector3(1.0, 1.0, 0.0);
	const Vector3 COLOR_CYAN = Vector3(0.0, 1.0, 1.0);

	sf::Window m_window;
	GLuint m_index;
	sf::Clock m_clock;
	sf::Time m_elapsed;
	float m_rotationAngle = 0.0f;
	bool m_flip = false;
	int m_current = 1;
	Vector3 m_vertices[8];
	Matrix3 m_rotate;
	Matrix3 m_translate;
	Matrix3 m_scale;

	const double TRANSLATION = 0.1;
};