# README #

This is OpenGL Practical 6

### What is this repository for? ###

* Practical 6 - Experimentation with a Cube in OpenGL
* Version 2.1

### How do I get set up? ###

* Ensure the SFML_SDK environment variable exists
* Ensure the OPENGL_SDK environment variable exists
* ensure SFML Version SFML 2.3.2 Visual C++ 14 (2015) - 32-bit is installed
* ([http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip "SFML-2.3.2-windows-vc14-32-bit.zip"](http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip))
* ensure GLEW OpenGL Wrangler Extension library - v1.13.0 win32 is downloaded
* ([http://sourceforge.net/projects/glew/files/glew/1.13.0/glew-1.13.0-win32.zip/download "glew-1.13.0-win32.zip"](http://sourceforge.net/projects/glew/files/glew/1.13.0/glew-1.13.0-win32.zip/download))
* Ensure the GLEW_SDK environment variable exists

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)