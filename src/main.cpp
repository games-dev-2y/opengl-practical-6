/// <summary>
/// @mainpage OpenGL Practical 6
/// @Author Rafael Girao
/// @Version 1.0
/// @brief Program will
///
/// Time Spent:
///		24th/11/2016 13:00 120min (2hr)
///
/// Total Time Taken:
///		2hr 00min
/// </summary>

#include "Game.h"

int main(void)
{
	Game & game = Game();
	game.run();
	return EXIT_SUCCESS;
}