#include "Game.h"

/// <summary>
/// Game constructor,
/// used to initialize all game members
/// </summary>
Game::Game() :
	m_window(sf::VideoMode(800, 600), "OpenGL Cube"),
	primitives(1),
	m_vertices()
{
	m_vertices[0] = Vector3(1.0, 1.0, 1.0);		//Right-Top-Front 0
	m_vertices[1] = Vector3(-1.0, 1.0, 1.0);	//Left-Top-Front 1
	m_vertices[2] = Vector3(-1.0, -1.0, 1.0);	//Left-Bottom-Front 2
	m_vertices[3] = Vector3(1.0, -1.0, 1.0);	//Right-Bottom-Front 3
	m_vertices[4] = Vector3(1.0, 1.0, -1.0);	//Right-Top-Back 4
	m_vertices[5] = Vector3(-1.0, 1.0, -1.0);	//Left-Top-Back 5
	m_vertices[6] = Vector3(-1.0, -1.0, -1.0);	//Left-Bottom-Back 6
	m_vertices[7] = Vector3(1.0, -1.0, -1.0);	//Right-Bottom-Back 7

	m_index = glGenLists(primitives);

	isRunning = true;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);

	// glNewList(index, GL_COMPILE);
	// Creates a new Display List
	// Initalizes and Compiled to GPU
	// https://www.opengl.org/sdk/docs/man2/xhtml/glNewList.xml
	glNewList(m_index, GL_COMPILE);
	glBegin(GL_QUADS);
	{

		//Back Face
		glColor3d(COLOR_GREEN.getX(), COLOR_GREEN.getY(), COLOR_GREEN.getZ());
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back

		//Right Face
		glColor3f(COLOR_RED.getX(), COLOR_RED.getY(), COLOR_RED.getZ()); // Red
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back

		//Left Face
		glColor3f(COLOR_YELLOW.getX(), COLOR_YELLOW.getY(), COLOR_YELLOW.getZ()); // Yellow
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back

		//Top Face
		glColor3f(COLOR_CYAN.getX(), COLOR_CYAN.getY(), COLOR_CYAN.getZ()); // Cyan
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front

		//Bottom Face
		glColor3f(COLOR_GREEN.getX(), COLOR_GREEN.getY(), COLOR_GREEN.getZ()); // Green
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front

		//Front Face
		glColor3f(COLOR_WHITE.getX(), COLOR_WHITE.getY(), COLOR_WHITE.getZ()); // White
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front


	}
	glEnd();
	glEndList();
}

/// <summary>
/// Game destructor,
/// used to get rid of any memory on the heap
/// </summary>
Game::~Game(){}

/// <summary>
/// Main entry point of the program
/// </summary>
void Game::run()
{
	sf::Event event;

	while (isRunning){

		std::cout << "Game running..." << std::endl;

		while (m_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				isRunning = false;
			}
			processGameEvents(event);
		}
		update();
		draw();
	}

}

/// <summary>
/// Will process game window events
/// </summary>
/// <param name="event"> event container </param>
void Game::processGameEvents(const sf::Event & event)
{
	switch (event.type)
	{
	case sf::Event::KeyPressed:
		switch (event.key.code)
		{
		case sf::Keyboard::Right:
			m_rotate = Matrix3::rotation(Matrix3::Axis::Y, 1.0);
			for (auto & vertex : m_vertices)
			{
				vertex = m_rotate * vertex;
			}
			break;
		case sf::Keyboard::Left:
			m_rotate = Matrix3::rotation(Matrix3::Axis::Y, -1.0);
			for (auto & vertex : m_vertices)
			{
				vertex = m_rotate * vertex;
			}
			break;
		case sf::Keyboard::Up:
			m_scale = Matrix3::scale(1.1, 1.1, 1.1);
			for (auto & vertex : m_vertices)
			{
				vertex = m_scale * vertex;
			}
			break;
		case sf::Keyboard::Down:
			m_scale = Matrix3::scale(0.9, 0.9, 0.9);
			for (auto & vertex : m_vertices)
			{
				vertex = m_scale * vertex;
			}
			break;
		case sf::Keyboard::W:
			m_translate = Matrix3::translate(0.0, -TRANSLATION);
			for (auto & vertex : m_vertices)
			{
				vertex = m_translate * vertex;
			}
			break;
		case sf::Keyboard::S:
			m_translate = Matrix3::translate(0.0, TRANSLATION);
			for (auto & vertex : m_vertices)
			{
				vertex = m_translate * vertex;
			}
			break;
		case sf::Keyboard::A:
			m_translate = Matrix3::translate(-TRANSLATION, 0.0);
			for (auto & vertex : m_vertices)
			{
				vertex = m_translate * vertex;
			}
			break;
		case sf::Keyboard::D:
			m_translate = Matrix3::translate(TRANSLATION, 0.0);
			for (auto & vertex : m_vertices)
			{
				vertex = m_translate * vertex;
			}
			break;
		default:
			break;
		}
		break;
	case sf::Event::KeyReleased:
		break;
	default:
		break;
	}
}

/// <summary>
/// Main Game update logic
/// </summary>
void Game::update()
{
	m_elapsed = m_clock.getElapsedTime();

	if (m_elapsed.asSeconds() >= 1.0f)
	{
		m_clock.restart();

		if (!m_flip)
		{
			m_flip = true;
			m_current++;
			if (m_current > primitives)
			{
				m_current = 1;
			}
		}
		else
			m_flip = false;
	}

	if (m_flip)
	{
		m_rotationAngle += 0.005f;

		if (m_rotationAngle > 360.0f)
		{
			m_rotationAngle -= 360.0f;
		}
	}

	updateVertices();
	
	std::cout << "Update up" << std::endl;
}

void Game::updateVertices()
{
	glNewList(m_index, GL_COMPILE);
	glBegin(GL_QUADS);
	{

		//Back Face
		glColor3d(COLOR_GREEN.getX(), COLOR_GREEN.getY(), COLOR_GREEN.getZ());
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back

																						//Right Face
		glColor3f(COLOR_RED.getX(), COLOR_RED.getY(), COLOR_RED.getZ()); // Red
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back

																						//Left Face
		glColor3f(COLOR_YELLOW.getX(), COLOR_YELLOW.getY(), COLOR_YELLOW.getZ()); // Yellow
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back

																						//Top Face
		glColor3f(COLOR_CYAN.getX(), COLOR_CYAN.getY(), COLOR_CYAN.getZ()); // Cyan
		glVertex3d(m_vertices[4].getX(), m_vertices[4].getY(), m_vertices[4].getZ());	// Right-Top-Back
		glVertex3d(m_vertices[5].getX(), m_vertices[5].getY(), m_vertices[5].getZ());	// Left-Top-Back
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front

																						//Bottom Face
		glColor3f(COLOR_GREEN.getX(), COLOR_GREEN.getY(), COLOR_GREEN.getZ()); // Green
		glVertex3d(m_vertices[7].getX(), m_vertices[7].getY(), m_vertices[7].getZ());	// Right-Bottom-Back
		glVertex3d(m_vertices[6].getX(), m_vertices[6].getY(), m_vertices[6].getZ());	// Left-Bottom-Back
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front

																						//Front Face
		glColor3f(COLOR_WHITE.getX(), COLOR_WHITE.getY(), COLOR_WHITE.getZ()); // White
		glVertex3d(m_vertices[0].getX(), m_vertices[0].getY(), m_vertices[0].getZ());	// Right-Top-Front
		glVertex3d(m_vertices[1].getX(), m_vertices[1].getY(), m_vertices[1].getZ());	// Left-Top-Front
		glVertex3d(m_vertices[2].getX(), m_vertices[2].getY(), m_vertices[2].getZ());	// Left-Bottom-Front
		glVertex3d(m_vertices[3].getX(), m_vertices[3].getY(), m_vertices[3].getZ());	// Right-Bottom-Front


	}
	glEnd();
	glEndList();
}

/// <summary>
/// Main Game render logic
/// </summary>
void Game::draw()
{
	std::cout << "Drawing" << std::endl;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	std::cout << "Drawing Cube " << m_current << std::endl;
	glLoadIdentity();
	glRotatef(m_rotationAngle, 0, 0, 1); // Rotates the camera on Y Axis
	glTranslated(0.0, 0.0, -8.0);

	glCallList(m_current);

	m_window.display();

}